# MultiCD (version 20210409)

## Introduction

**multicd.sh** is a shell script designed to build a multiboot CD/DVD iso image containing many different Linux distributions and/or utilities.
Mine is a modified version of the [IsaacSchemm MultiCD](https://github.com/IsaacSchemm/MultiCD) fork, because of the original project seems to be temporarily or permanently abandoned, and because I wanted to make some small aesthetic improvements to the project. 

The advantages to making a bootable CD/DVD with this script are:

 - You don't need to burn multiple CDs/DVDs for small distributions.
 - If you already have the ISO images, it is not necessary to download them again.
 - When a new version of one of the distributions is released, you can simply download the new version and run the script again to build a newer multiboot image.

The ISO image that MultiCD writes can also be written to a flash drive, although the filesystem will be read-only; see the [ISOLINUX documentation](http://www.syslinux.org/wiki/index.php/Doc/isolinux#HYBRID_CD-ROM.2FHARD_DISK_MODE) for more info.

## Download multicd.sh repository

The repository can be download as a compressed file .zip, .tar.gz, .tar.bz2 or .tar (see the top of the page) and uncompress it.
You can also get multicd with the git command:

`
git clone https://gitlab.com/thegreatyellow67/multicd.git
`

## Instructions

1. See **Download source code** section above for download multicd.

2. Copy or symlink some of the supported boot images in the MultiCD folder.
	
	* In general, only distros supported by this script will work. However, you can also try using the "generic" plugin (see below.)
	
	* Floppy and hard drive images are supported - any image with a .img or .imz extension will be picked up.
	
	* For some distribuitons, you can use the original file name of the ISO, and MultiCD will create a symlink automatically. For others, you will have to rename the ISO to the name listed below.
	
	* Don't put a file inside the multicd folder and then make a symlink to it in that same folder, or multicd might delete the symlink.

3. Install mkisofs or genisoimage. If your distribution comes with CD/DVD burning software, you probably have it already.
**Note for Arch Linux users**: probably if you are an Arch Linux user you have to install packages **cdrtools** and **syslinux** if not installed previously.

4. Open a terminal, go to the MultiCD folder (made in step 1) and type:

	`
	user@pc$ chmod +x multicd.sh
	`

	`
	user@pc$ ./multicd.sh
	`

The script will detect which images are present and make an ISO build for you. They will appear on the menu of the final CD/DVD in the order they are listed.
(Note: if the final size is over 700 MB, you will have to use a DVD.)

## Arguments for multicd.sh

| argument           | argument scope                                                                                        |
| :----------------: | ----------------------------------------------------------------------------------------------------- |
| **-c**             | include an MD5 checksum file (md5sum.txt)                                                             |
| **-d**             | drop to a shell prompt before building the ISO (debug)                                                |
| **-m**             | don't include Memtest86+                                                                              |
| **-i**             | offer options like ISOLINUX menu color or copying only certain Slax modules (requires dialog command) |
| **-o [filename]**  | lets you use another filename for the output instead of multicd.iso                                   |
| **-t**             | run the ISO in QEMU after it is built                                                                 |
| **-v**             | be more verbose                                                                                       |
| **-V**             | print out the version number                                                                          |
| **-w**             | put a "press enter to continue" before exiting                                                        |
| **-help**          | displays available arguments                                                                          |
| **-clean**         | don't run the multicd.sh script at all - instead, get rid of the symlinks                             |
|                    | and temporary version files that it makes automatically                                               |
| **-download**      | open a dialog with a list of some current distributions available for download                        |

## "generic" plugin

MultiCD has two methods for copying the contents of a boot CD to the new image:

 - The standard method is to use a script specific to the boot disk being copied. These plugin scripts come with MultiCD and are listed on Supported Distros section. Most of these scripts copy files directly from the original ISO; this requires specific scripts for each ISO, but the advantage is that you can boot the distro from the MultiCD-generated disc without copying the whole distro to RAM.

 - If there's no script for your distro, or the script is too old, you can try using the "generic" plugin, which copies the whole ISO image as-is to the new CD/DVD and boots it with [memdisk](http://www.syslinux.org/wiki/index.php/MEMDISK). This requires you to copy the image to RAM whenever you want to run it, but it doesn't require any maintenance on my part (and you can remove the disk after everything is copied to memory.) Unfortunately, it won't work for most distros - live CDs usually look for a real disk, and don't know where to find the image that memdisk loads into memory.
  - To tell MultiCD to use the "generic" plugin, give the ISO a name that ends in .generic.iso.
  - You can also write a custom name to a .defaultname file - e.g. bootdisk.generic.defaultname for bootdisk.generic.iso - and this name will appear on the MultiCD menu. (.defaultname files will be removed by multicd.sh if you remove the corresponding ISO file.)

## Notes

- MultiCD can use the following methods to extract ISOs. The first two are preferred if they are available - instead of extracting the ISO and then copying files, they loop-mount the image, so files are only copied once:

	* Linux loop-mount (only as root on Linux kernel)

	* fuseiso

	* 7z (also supported on Cygwin!)

	* ark

	* file-roller

- You can use more than one ISO for certain distros, such as Ubuntu and/or Linux Mint, without using the "generic" plugin. Either name them [anything].ubuntu.iso for Ubuntu / [anything].linuxmint.iso for Mint, or in some cases you can keep the original ISO filenames (and MultiCD will make temporary symlinks to the names it expects to see.)
You can configure the menu names of these distros by editing the .defaultname file (i.e. [anything].ubuntu.defaultname) or running MultiCD with the "-i" option.

- If MultiCD is hogging your hard drive usage, try [switching](http://blog.nexcess.net/2010/11/07/changing-your-linux-io-scheduler/) your drive to the cfq scheduler. (cfq supports ionice, which is run near the beginning of multicd.sh and tells the system to give MultiCD low priority on the hard disk.) You'll usually only need to do this on systems with traditional hard drives (not SSDs).

## Supported Distros

In most cases, you can use the original filename of the ISO and it will be detected by MultiCD, or you can use the filename in this list.

If the ISO you want to use is not in the list below, or if MultiCD's plugin for that ISO is too old to work, you can try using the generic plugin instead, which will load the ISO into RAM and boot it with memdisk. To use the generic plugin, name the ISO [something].generic.iso.

Multiple versions of Debian live CDs, Ubuntu live CDs, and Puppy variants can exist on a single disk. The generic plugin (if it works) can also allow several variants of the same distro to coexist.

Note: these distributions will appear in alphabetical order, by the names of the plugin .sh files. Change the filenames of the files in the plugins folder to change the order.

| **Distro name**                                 | **ISO alias**                                                       | **Link**                                                  |
| :---------------------------------------------: | ------------------------------------------------------------------- | ----------------------------------------------------------|
| **Android-x86**                                 | android-x86-*.iso                                                   | [Download](http://www.android-x86.org/)                   |
| **antiX**                                       | antix.iso                                                           | [Download](https://antixlinux.com/download/)              |
| **Arch Linux FTP or CORE**                      | arch.iso                                                            | [Download](https://archlinux.org/download/)               |
| **Archboot**                                    | archboot.iso                                                        | [Download](https://wiki.archlinux.org/index.php/Archboot) |
| **Avira AntiVir Rescue System**                 | avira.iso                                                           | [Download](http://www.avira.com/en/support-download-avira-antivir-rescue-system)|
| **Boot-Repair-Disk**                            | boot-repair-disk-*.iso                                              | [Download](https://sourceforge.net/projects/boot-repair-cd/files/)|
| **CAINE (Forensics Linux)**                     | caine.iso                                                           | [Download](https://www.caine-live.net/)                   |
| **CDlinux**                                     | cdl.iso                                                             | [Download](https://sourceforge.net/projects/cd-linux/files/)|
| **CentOS boot/netinstall ISO**                  | centos-boot.iso                                                     | [Download](https://www.centos.org/download/)              |
| **Clonezilla**                                  | clonezilla.iso                                                      | [Download](https://clonezilla.org/downloads.php)          |
| **Comodo Rescue Disk**                          | comodo_rescue_disk_*.iso                                            | [Download](https://www.comodo.com/business-security/network-protection/rescue-disk.php)|
| **ConnochaetOS**                                | connos.iso                                                          | [Download](https://sourceforge.net/projects/connochaetos/files/)|
| **DBAN (official or unofficial)**               | dban.iso                                                            | [Download](https://github.com/NHellFire/dban/releases)    |
| **Debian Live**                                 | *.debian.iso or binary.iso                                          | [Download](https://www.debian.org/CD/live/)               |
| **Debian netinst**                              | debian-install.iso or debian-install64.iso                          | [Download](https://www.debian.org/distrib/netinst)        |
| **Debian (stable, testing or unstable)**        | debian-mini.iso or debian-mini64.iso                                | [Download](https://www.debian.org/CD/http-ftp/)           |
| **Damn Small Linux**                            | dsl.iso                                                             | [Download](http://www.damnsmalllinux.org/)                |
| **Dr.Web LiveCD**                               | drweb.iso                                                           | [Download](https://free.drweb.com/aid_admin/)             |
| **DoudouLinux**                                 | doudoulinux.iso                                                     | [Download](http://www.doudoulinux.org/web/english/)       |
| **Endian Firewall**                             | efw.iso                                                             | [Download](https://sourceforge.net/projects/efw/files/)   |
| **Feather**                                     | feather.iso                                                         | [Download](https://sourceforge.net/projects/archiveos/files/f/feather/)|
| **Fedora (x86_64)**                             | fedora.iso                                                          | [Download](https://getfedora.org/en/workstation/download/)|
| **Fedora netinst (x86_64)**                     | fedora-boot64.iso                                                   | [Download](https://getfedora.org/en/workstation/download/)|
| **Finnix**                                      | finnix.iso                                                          | [Download](https://www.finnix.org/)                       |
| **FreeDOS base or full CD**                     | fdbasecd.iso/fdfullcd.iso                                           | [Download](https://www.freedos.org/)                      |
| **Gentoo**                                      | gentoo.iso                                                          | [Download](https://www.gentoo.org/downloads/)             |
| **GParted Live**                                | gparted.iso                                                         | [Download](https://gparted.org/download.php)              |
| **GRUB4DOS grub.exe**                           | grub.exe                                                            | [Download](https://sourceforge.net/projects/grub4dos/files/)|
| **grml (x86, x64 or combined)**                 | grml.iso                                                            | [Download](https://grml.org/download/)                    |
| **Hiren's BootCD PE**                           | hirens.iso                                                          | [Download](https://www.hirensbootcd.org/download/)        |
| **IPCop**                                       | ipcop.iso                                                           | [Download](http://www.ipcop.org/download.html)            |
| **Kali Linux**                                  | kali-linux-2*.iso (or kali-linux-mini-*.iso for the mini ISO)       | [Download](https://www.kali.org/downloads/)               |
| **Kaspersky Rescue Disk**                       | krd.iso                                                             | [Download](https://www.kaspersky.com/downloads/thank-you/free-rescue-disk)|
| **Knoppix**                                     | knoppix.iso                                                         | [Download](http://www.knoppix.org/)                       |
|                                                 | (Versions 5 and 6 supported. If you use the DVD version,            |                                                           |
|                                                 | KNOPPIX2 will be dropped.)                                          |                                                           |
| **Linux Lite**                                  | linux-lite-*.iso                                                    | [Download](https://www.linuxliteos.com/download.php)      |
| **Linux Mint**                                  | linuxmint.iso                                                       | [Download](https://www.linuxmint.com/download.php)        |
| **Linux Mint Debian Edition**                   | linuxmint.iso                                                       | [Download](https://www.linuxmint.com/download_lmde.php)   |
| **Macpup**                                      | macpup.iso                                                          | [Download](https://www.macpup.org/)                       |
| **Mageia (live CD/DVD)**                        | mageia.iso                                                          | [Download](https://www.mageia.org/en/downloads/)          |
| **Mageia (boot.iso)**                           | mageia-boot.iso, mageia-boot-2.iso                                  | [Download](https://www.mageia.org/en/downloads/)          |
| **Manjaro**                                     | manjaro.iso                                                         | [Download](https://manjaro.org/)                          |
| **Maui**                                        | maui-*.iso                                                          | [Download](https://mauilinux.org/download/)               |
| **NetbootCD**                                   | netbootcd.iso                                                       | [Download](https://github.com/IsaacSchemm/netbootcd/releases/)|
| **netboot.xyz**                                 | netboot.xyz-*.iso (or *.generic.iso)                                | [Download](https://netboot.xyz/downloads/)                |
| **Offline NT Password & Registry Editor**       | ntpasswd.iso                                                        | [Download](http://pogostick.net/~pnh/ntpasswd/)           |
| **openSUSE tumbleweed (i686) NET**              | opensuse-net.iso                                                    | [Download](https://get.opensuse.org/tumbleweed)           |
| **openSUSE tumbleweed (x86_64) NET**            | opensuse-net64.iso                                                  | [Download](https://get.opensuse.org/tumbleweed)           |
| **openSUSE leap (x86_64) NET**                  | opensuse-net64.iso                                                  | [Download](https://get.opensuse.org/leap)           |
| **Ophcrack**                                    | ophxp.iso / ophvista.iso                                            | [Download](https://ophcrack.sourceforge.io/)              |
| **Parabola GNU/Linux-libre**                    | parabola.iso                                                        | [Download](https://wiki.parabola.nu/Get_Parabola)         |
| **PCLinuxOS**                                   | pclos.iso*                                                          | [Download](https://www.pclinuxos.com/)                    |
| **PCLinuxOS LXDE**                              | pclx.iso*                                                           | [Download](https://www.pclinuxos.com/)                    |
|                                                 | * Note: The two plugins can both be used for any other              |                                                           |
|                                                 | version of PCLinuxOS                                                |                                                           |
| **Pentoo**                                      | pentoo.iso                                                          | [Download](https://www.pentoo.ch/downloads)               |
| **PING**                                        | ping.iso                                                            | [Download](http://ping.windowsdream.com/#download)        |
| **Pinguy OS**                                   | pinguy.iso                                                          | [Download](http://pinguy-os.sourceforge.net/)             |
| **Parted Magic**                                | pmagic.iso                                                          | [Download](https://partedmagic.com/store/)                |
| **Porteus**                                     | porteus.iso                                                         | [Download](http://porteus.org/)                           |
| **Porteus Kiosk**                               | porteus.iso                                                         | [Download](http://porteus-kiosk.org/)                     |
| **Porteus modules**                             | *.xzm                                                               |                                                           |
| **Puppy**                                       | puppy.iso                                                           | [Download](http://www.puppylinux.com/)                    |
| **Redo Backup (now Redo Rescue)**               | redobackup-livecd-*.iso                                             | [Download](http://redorescue.com/)                        |
| **Rescatux**                                    | rescatux.iso                                                        | [Download](https://www.supergrubdisk.org/rescatux/)       |
| **ScientificLinux boot/netinstall ISO**         | scientific-boot.iso                                                 | [Download](https://scientificlinux.org/downloads/)        |
| **Sabayon Linux**                               | sabayon.iso                                                         | [Download](https://www.sabayon.org/download/)             |
| **Slax**                                        | slax.iso                                                            | [Download](https://www.slax.org/#purchase)                |
| **Slax modules**                                | "*.lzm"                                                             |                                                           |
| **SliTaz**                                      | slitaz.iso                                                          | [Download](https://www.slitaz.org/en/)                    |
| **SystemRescueCd**                              | sysresccd.iso                                                       | [Download](https://www.system-rescue.org/Download/)       |
| **Tails**                                       | tails.iso                                                           | [Download](https://tails.boum.org/install/index.en.html)  |
| **Tiny Core Linux**                             | tinycore.iso                                                        | [Download](http://www.tinycorelinux.net/downloads.html)   |
| **Trinity Rescue Kit**                          | trk.iso                                                             | [Download](https://trinityhome.org/trinity_rescue_kit_download/)|
| **Trisquel GNU/Linux**                          | trisquel.iso (or original filename)                                 | [Download](https://trisquel.info/)                        |
| **Ubuntu, Ubuntu Server live CDs/DVDs**         | original filename                                                   | [Download](https://ubuntu.com/#download)                  |
| **Kubuntu live CDs/DVDs**                       | original filename                                                   | [Download](https://kubuntu.org/getkubuntu/)               |
| **Xubuntu live CDs/DVDs**                       | original filename                                                   | [Download](https://xubuntu.org/download)                  |
| **Lubuntu live CDs/DVDs**                       | original filename                                                   | [Download](https://lubuntu.net/downloads/)                |
| **Other Ubuntu Live CDs/DVDs**                  | *.casper.iso                                                        |                                                           |
| **Ubuntu alternate/server non-live installer**  | *.ubuntu-alternate.iso (or original filename)                       |                                                           |
| **Ubuntu mini.iso (network installer)**         | ubuntu-mini.iso                                                     |                                                           |
| **Ultimate Boot CD**                            | ubcd.iso                                                            | [Download](https://www.ultimatebootcd.com/download.html)  |
| **Windows 7 Recovery Disc**                     | win7recovery.iso                                                    |                                                           |
| **Windows 98 SE**                               | win98se.iso                                                         |                                                           |
| **Windows Me**                                  | winme.iso                                                           |                                                           |
| **Zorin OS**                                    | zorin.iso                                                           | [Download](https://zorinos.com/download/)                 |
| **Any floppy disk image**                       | *.img or *.imz (could be Super Grub Disk, MS-DOS, etc.)             | [Download](https://www.supergrubdisk.org/)                |
|                                                 | (The newest Super Grub2 Disk comes with an .iso extension, but it   |                                                           |
|                                                 | will also work as a floppy image. Change the extension to .img to   |                                                           |
|                                                 | use it with MultiCD.                                                |                                                           |
| **Any floppy disk image**                       | games/*.img or games/*.imz (for bootable DOS disk images with games)|                                                           |
| **Memtest86+**                                  | automatic                                                           | [Download](https://www.memtest.org/)                      |
